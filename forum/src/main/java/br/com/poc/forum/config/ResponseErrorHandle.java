package br.com.poc.forum.config;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import br.com.poc.forum.DTO.ErrorDTO;

@RestControllerAdvice
public class ResponseErrorHandle {
	
	@Autowired
	MessageSource messageSource;
	
	@ResponseStatus(code = HttpStatus.BAD_REQUEST)
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public List<ErrorDTO> handle(MethodArgumentNotValidException methodArgument){
			
		List<ErrorDTO> errors = new ArrayList<>();
		List<FieldError> errorList = methodArgument.getBindingResult().getFieldErrors();
		errorList.forEach(error -> {
			String message = messageSource.getMessage(error, LocaleContextHolder.getLocale());
			ErrorDTO errorDTO = new ErrorDTO(error.getField(), message);
			errors.add(errorDTO);
		});
		return errors;
	}

}

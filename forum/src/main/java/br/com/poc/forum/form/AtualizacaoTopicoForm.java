package br.com.poc.forum.form;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import br.com.poc.forum.modelo.Topico;
import br.com.poc.forum.repository.ForumRepository;

public class AtualizacaoTopicoForm {

	@NotNull 
	@NotEmpty
	@Length(min = 3)
	private String titulo;
	
	@NotNull
	@NotEmpty
	@Length(min = 10)
	private String mensagem;

	public AtualizacaoTopicoForm(@NotNull @NotEmpty @Length(min = 3) String titulo,
			@NotNull @NotEmpty @Length(min = 10) String mensagem) {
		this.titulo = titulo;
		this.mensagem = mensagem;
	}

	public String getTitulo() {
		return titulo;
	}

	public String getMensagem() {
		return mensagem;
	}

	public Topico atualizar(Long id, ForumRepository forumRepository) {
		Topico topico = forumRepository.getOne(id);
		topico.setTitulo(this.titulo);
		topico.setMensagem(this.mensagem);
		
		return topico;
	}
	
	
}

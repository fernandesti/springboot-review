package br.com.poc.forum.DTO;

public class ErrorDTO {

	private String filter;
	private String message;
	
	public ErrorDTO(String filter, String message) {
		this.filter = filter;
		this.message = message;
	};
	
	public String getFilter() {
		return filter;
	}

	public String getMessage() {
		return message;
	}

	
	
}

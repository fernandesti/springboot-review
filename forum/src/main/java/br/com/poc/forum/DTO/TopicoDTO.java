package br.com.poc.forum.DTO;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import br.com.poc.forum.modelo.Topico;

public class TopicoDTO {
	
	private Long id;
	private String titulo;
	private String messagem;
	private LocalDateTime dataCriacao;
	
	public TopicoDTO(Topico topico) {
		this.id = topico.getId();
		this.titulo = topico.getTitulo();
		this.messagem = topico.getMensagem();
		this.dataCriacao = topico.getDataCriacao();
	}
	
	public Long getId() {
		return id;
	}
	public String getTitulo() {
		return titulo;
	}
	public String getMessagem() {
		return messagem;
	}
	public LocalDateTime getDataCriacao() {
		return dataCriacao;
	}
	
	
	public static List<TopicoDTO> converterToTopicoDTO(List<Topico> listaTopico){
		return listaTopico.stream().map(TopicoDTO::new).collect(Collectors.toList());
	}
}

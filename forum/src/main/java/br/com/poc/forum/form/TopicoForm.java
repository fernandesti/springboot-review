package br.com.poc.forum.form;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import br.com.poc.forum.modelo.Curso;
import br.com.poc.forum.modelo.Topico;
import br.com.poc.forum.repository.CursoRepository;

public class TopicoForm {
	
	@NotNull @NotEmpty @Size(min = 3, max = 30)
	public String titulo;
	
	@NotNull @NotEmpty @Size(min = 10, max = 255)
	public String mensagem;
	
	@NotNull @NotEmpty
	public String nomeCurso;
	
	public Topico converterToTopico(CursoRepository cursoRepository)
	{
		Curso curso = cursoRepository.findByNome(nomeCurso);
		return new Topico(titulo, mensagem, curso);
	}
			

}

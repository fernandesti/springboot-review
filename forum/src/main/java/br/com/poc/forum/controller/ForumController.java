package br.com.poc.forum.controller;

import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.function.EntityResponse;
import org.springframework.web.util.UriComponentsBuilder;

import br.com.poc.forum.DTO.DetalheTopicoDTO;
import br.com.poc.forum.DTO.TopicoDTO;
import br.com.poc.forum.form.AtualizacaoTopicoForm;
import br.com.poc.forum.form.TopicoForm;
import br.com.poc.forum.modelo.Curso;
import br.com.poc.forum.modelo.Resposta;
import br.com.poc.forum.modelo.Topico;
import br.com.poc.forum.repository.CursoRepository;
import br.com.poc.forum.repository.ForumRepository;
import jdk.jfr.BooleanFlag;

@RestController
@RequestMapping("/topicos")
public class ForumController {
	
	@Autowired
	private ForumRepository forumRepository;
	
	@Autowired
	private CursoRepository cursoRepository;

	
	@GetMapping
	public List<TopicoDTO> topicos(String nomeCurso){
		//Topico t1 = new Topico("Node", "Js no back-end", new Curso("NodeJs", "Programação"));
		//List<Topico> topicos = Arrays.asList(t1, t1, t1);
		
		List<Topico> topicos = new ArrayList<Topico>();
		if(nomeCurso == null) {
			topicos = forumRepository.findAll();
		}else {
			topicos = forumRepository.findByCursoNome(nomeCurso);
		}
		
		//List<Topico> topicos = forumRepository.findAll();
		//return topicos.stream().map(TopicoDTO::new).collect(Collectors.toList());
		
		return TopicoDTO.converterToTopicoDTO(topicos);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<DetalheTopicoDTO>  topicos(@PathVariable Long id)
	{
		Optional<Topico> topico = forumRepository.findById(id);
		if(topico.isPresent()) {
			return ResponseEntity.ok(new DetalheTopicoDTO(topico.get()));
		}
		return ResponseEntity.notFound().build();
	}
	
	@PostMapping
	@Transactional
	public ResponseEntity<TopicoDTO> adicionar(@Valid @RequestBody TopicoForm form, UriComponentsBuilder uriBuilder) {
		Topico topico = form.converterToTopico(cursoRepository);
		
		forumRepository.save(topico);
		
		URI uri = uriBuilder.path("/topicos/{id}").buildAndExpand(topico.getId()).toUri();
		
		return ResponseEntity.created(uri).body(new TopicoDTO(topico));
	}
	
	@PutMapping("/{id}")
	@Transactional
	public ResponseEntity<TopicoDTO> atualizar(@PathVariable Long id, @Valid @RequestBody AtualizacaoTopicoForm form){
		
		Optional<Topico> opt = forumRepository.findById(id);
		if(opt.isPresent()) {
			Topico topico = form.atualizar(id, forumRepository);
			return ResponseEntity.ok(new TopicoDTO(topico));
		}
		return ResponseEntity.notFound().build();
	}

	@DeleteMapping("/{id}")
	@Transactional
	public ResponseEntity<?> remover(@PathVariable Long id){
		
		Optional<Topico> opt = forumRepository.findById(id);
		if(opt.isPresent()) {
			forumRepository.deleteById(id);
			return ResponseEntity.noContent().build();
		}
		
		return ResponseEntity.notFound().build();
	}
}

package br.com.poc.forum.DTO;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import br.com.poc.forum.modelo.Resposta;
import br.com.poc.forum.modelo.StatusTopico;
import br.com.poc.forum.modelo.Topico;

public class DetalheTopicoDTO {

	private Long id;
	private String titulo;
	private String messagem;
	private LocalDateTime dataCriacao;
	private String nomeAutor;
	private StatusTopico status = StatusTopico.NAO_RESPONDIDO;
	private List<RespostaDTO> respostas;
	
	public DetalheTopicoDTO(Topico topico) {
		this.id = topico.getId();
		this.titulo = topico.getTitulo();
		this.messagem = topico.getMensagem();
		this.dataCriacao = topico.getDataCriacao();
		this.nomeAutor = topico.getAutor().getNome();
		this.status = topico.getStatus();
		this.respostas = new ArrayList<>();
		this.respostas.addAll(topico.getRespostas().stream().map(RespostaDTO::new).collect(Collectors.toList()));
	}

	public Long getId() {
		return id;
	}

	public String getTitulo() {
		return titulo;
	}

	public String getMessagem() {
		return messagem;
	}

	public LocalDateTime getDataCriacao() {
		return dataCriacao;
	}

	public String getNomeAutor() {
		return nomeAutor;
	}

	public StatusTopico getStatus() {
		return status;
	}

	public List<RespostaDTO> getRespostas() {
		return respostas;
	}
	
	
}
